import java.util.HashMap;

import org.bukkit.craftbukkit.v1_6_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import net.minecraft.server.v1_6_R3.Packet;
import net.minecraft.server.v1_6_R3.Packet206SetScoreboardObjective;
import net.minecraft.server.v1_6_R3.Packet207SetScoreboardScore;
import net.minecraft.server.v1_6_R3.Packet208SetScoreboardDisplayObjective;
import net.minecraft.server.v1_6_R3.Scoreboard;
import net.minecraft.server.v1_6_R3.ScoreboardBaseCriteria;
import net.minecraft.server.v1_6_R3.ScoreboardScore;

public class ScoreManager {
	public HashMap<String , Scoreboard> scoreBoards = new HashMap<String , Scoreboard>();
	
	public void newScoreBoard(final Plugin plugin,final Player player,final String name){
		plugin.getServer().getScheduler().runTask(plugin, new Runnable() {
			@Override
			public void run() {
				Scoreboard sb = new Scoreboard();
				sb.registerObjective(name, new ScoreboardBaseCriteria(name));
				if(!scoreBoards.containsKey(name)){
					scoreBoards.put(name , sb);
				}

				Packet206SetScoreboardObjective packet = new Packet206SetScoreboardObjective(sb.getObjective(name), 0);
				Packet208SetScoreboardDisplayObjective display = new Packet208SetScoreboardDisplayObjective(1, sb.getObjective(name));

				sendPacket(player, packet);
				sendPacket(player, display);
			}

		});
	}

	public void newScore(final Plugin plugin,final Player player,final String scoreboard, final String scorename, final int score){
		plugin.getServer().getScheduler().runTask(plugin, new Runnable() {
			@Override
			public void run() {
				if(scoreBoards.containsKey(scoreboard)){
					Scoreboard sb = scoreBoards.get(scoreboard);

					ScoreboardScore scoreItem = sb.getPlayerScoreForObjective(scorename, sb.getObjective(scoreboard));
					scoreItem.setScore(score);

					Packet207SetScoreboardScore pScoreItem = new Packet207SetScoreboardScore(scoreItem, 0);
					sendPacket(player, pScoreItem);
				}
			}
		});
	}
	
	public void removeScore(final Plugin plugin,final Player player,final String scoreboard, final String scorename){
		plugin.getServer().getScheduler().runTask(plugin, new Runnable() {
			@Override
			public void run() {
				if(scoreBoards.containsKey(scoreboard)){
					Scoreboard sb = scoreBoards.get(scoreboard);

					ScoreboardScore scoreItem = sb.getPlayerScoreForObjective(scorename, sb.getObjective(scoreboard));

					Packet207SetScoreboardScore pScoreItem = new Packet207SetScoreboardScore(scoreItem, 1);
					sendPacket(player, pScoreItem);
				}
			}
		});
	}




    public static void sendPacket(Player player, Packet packet) {
    	((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }
}
